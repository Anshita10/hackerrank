"""Mr. Vincent works in a door mat manufacturing company. One day, he designed a new door mat with the following specifications:

Mat size must be X. ( is an odd natural number, and  is  times .)
The design should have 'WELCOME' written in the center.
The design pattern should only use |, . and - characters.
Sample Designs

    Size: 7 x 21 
    ---------.|.---------
    ------.|..|..|.------
    ---.|..|..|..|..|.---
    -------WELCOME-------
    ---.|..|..|..|..|.---
    ------.|..|..|.------
    ---------.|.---------
    
    Size: 11 x 33
    ---------------.|.---------------
    ------------.|..|..|.------------
    ---------.|..|..|..|..|.---------
    ------.|..|..|..|..|..|..|.------
    ---.|..|..|..|..|..|..|..|..|.---
    -------------WELCOME-------------
    ---.|..|..|..|..|..|..|..|..|.---
    ------.|..|..|..|..|..|..|.------
    ---------.|..|..|..|..|.---------
    ------------.|..|..|.------------
    ---------------.|.---------------
Input Format

A single line containing the space separated values of  and .

Constraints

Output Format

Output the design pattern.

Sample Input

9 27
Sample Output

------------.|.------------
---------.|..|..|.---------
------.|..|..|..|..|.------
---.|..|..|..|..|..|..|.---
----------WELCOME----------
---.|..|..|..|..|..|..|.---
------.|..|..|..|..|.------
---------.|..|..|.---------
------------.|.------------"""

N,M=input().split()
N=int(N)
M=int(M)
k=1
for i in range (N):
       if(i<(N+1)/2-1):
        print('-'*(int((M-3*k)/2)),end='')
        print('.|.'*k,end='')
        print('-'*(int((M-3*k)/2)))
        k+=2
       elif i==(N+1)/2-1:
        print('-'*int((M-7)/2),"WELCOME",'-'*int((M-7)/2),sep="")
       else:
        k-=2
        print('-'*int((M-3*k)/2),end='')
        print('.|.'*k,end='')
        print('-'*int((M-3*k)/2))
        

#Input:7 21
#Output:---------.|.---------
   #    ------.|..|..|.------
   #    ---.|..|..|..|..|.---
   #    -------WELCOME-------
   #    ---.|..|..|..|..|.---
   #    ------.|..|..|.------
   #    ---------.|.---------
